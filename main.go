package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"regexp"
	"strings"

	"database/sql"

	_ "gopkg.in/goracle.v2"

	log "github.com/sirupsen/logrus"

	endpt "github.com/kubernetes-incubator/external-dns/endpoint"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
)

var (
	groups           arrayFlags
	config			 *rest.Config
	connectionString = flag.String("oracle-connection-string", "default", "Connection string to oracle database")
	sizeOfCr         = flag.Int("crd-half-full", 5000, "Number of endpoints allowed in a CRs before creating a new one")
	namespace        = flag.String("namespace", "paas-infra-dnsmanager", "Namespace where the CRs are going to be created")
	kind             = flag.String("kind", "DNSEndpoint", "Kind of the CRs that are going to be created")
	resourcePlural   = strings.ToLower(*kind) + "s"
	version          = flag.String("version", "v1alpha1", "ApiVersion of the CRs that are going to be created")
	nameOfRecords    = flag.String("name", "record", "Name that the CRs will have that will be concatenated with their number e.g record-1, record-2...")
)

func main() {

	flag.Var(&groups, "group", "Group to the CR to be created")

	// Parses the command line into the defined flags
	flag.Parse()

	if *connectionString == "default" {
		log.Fatal("A connectoin string was not provided, please provide one")
	}

	// Input sanitization
	for _, group := range groups {
		if !strings.HasSuffix(group, ".cern.ch") || !strings.HasPrefix(group, "internal") && !strings.HasPrefix(group, "external") {
			log.Fatal("Invalid group in arguments doesn't end with .cern.ch or it doen's start with either interal or external: " + group)
		}
	}

	var err error
	// creates the in-cluster config
	config, err = rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}
	config.APIPath = "/apis"
	config.NegotiatedSerializer = serializer.WithoutConversionCodecFactory{CodecFactory: scheme.Codecs}
	config.UserAgent = rest.DefaultKubernetesUserAgent()


	// Open connectoin to DB in connection string
	db, err := sql.Open("goracle", *connectionString)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer db.Close()

	// Todo remove the 20 rows limiter
	rows, err := db.Query("SELECT * FROM \"WEBREG\".\"ZONE_WEB\" WHERE rownum <= 20")
	if err != nil {
		fmt.Println("Error running query")
		fmt.Println(err)
		return
	}
	defer rows.Close()

	// Filling the zone structure
	zone := map[string][]string{}

	var alias, name, domain string
	for rows.Next() {
		if err := rows.Scan(&alias, &name, &domain); err != nil {
			log.Fatal(err)
		}
		alias = strings.ToLower(alias)
		name = strings.ToLower(name)
		domain = strings.ToLower(domain)

		if _, ok := zone[alias]; ok {
			zone[alias] = append(zone[alias], name+domain)
		} else {
			zone[alias] = []string{name + domain}
		}
	}

	log.Info("Zone struture built and populated")

	// Processing the zones
	generateCRs(groups, zone)

}

func generateCRs(groups []string, zone map[string][]string) {
	for _, group := range groups {
		splitedGroup := strings.Split(group, ".")
		desiredZone := strings.Join(splitedGroup[1:], ".")

		var nbOfCRs int
		log.Info("Going to process ", desiredZone, " with ", len(zone), " aliases")
		if nbOfCRs = len(zone) / *sizeOfCr; nbOfCRs == 0 && len(zone) > 0 {
			nbOfCRs = 1
		}

		var endpoints []*endpt.Endpoint
		i, j := 0, 0
		for alias, targets := range zone {
			endpoints = append(endpoints, createEndpoint(alias+"."+desiredZone, 60, targets))
			j++

			if j%*sizeOfCr == 0 {
				name := fmt.Sprintf("%s-%d", "record", i)
				dnsEndpoint := createDNSEndpoint(name, endpoints)
				dnsEndpoint.APIVersion = group + "/v1alpha1"

				createResource(group, fmt.Sprintf("%s-%s", group, name+".json"), dnsEndpoint)
				endpoints = []*endpt.Endpoint{}
				i++
			}
		}

		name := fmt.Sprintf("%s-%d", *nameOfRecords, i)
		dnsEndpoint := createDNSEndpoint(name, endpoints)
		dnsEndpoint.APIVersion = group + "/" + *version
		dnsEndpoint.Kind = *kind

		createResource(group, name, dnsEndpoint)
	}
}

func createResource(group string, name string, dnsEndpoint *endpt.DNSEndpoint) {
	// Writing resource to JSON
	data, err := json.MarshalIndent(dnsEndpoint, "", " ")
	if err != nil {
		panic(err)
	}

	// Setting up the group and version
	config.GroupVersion = &schema.GroupVersion{Group: group, Version: *version}

	// Creates the clientset
	clientset, err := rest.RESTClientFor(config)
	if err != nil {
		panic(err.Error())
	}

	// Attempts to create the resource
	result := clientset.
		Post().
		Namespace(*namespace).
		Resource(resourcePlural).
		Body(data).
		Do()

	var status int
	result.StatusCode(&status)
	
	// In case the object already exists lets try to delete it and then create it again
	if status == 409 {
		result = clientset.
			Delete().
			Namespace(*namespace).
			Resource(resourcePlural).
			Name(name).
			Do()
		result.StatusCode(&status)
		if status != 200{
			log.Fatal(result.Error().Error())
		}

		result = clientset.
			Post().
			Namespace(*namespace).
			Resource(resourcePlural).
			Body(data).
			Do()
		result.StatusCode(&status)
	}

	// Final result of the operation
	if status != 201{
		log.Fatal(result.Error().Error())
	}
	log.Info("Created CR of group ", group, " named ", name, " with ", len(dnsEndpoint.Spec.Endpoints), " endpoints")
}

func createDNSEndpoint(name string, endpoints []*endpt.Endpoint) *endpt.DNSEndpoint {
	return &endpt.DNSEndpoint{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: *namespace,
		},
		Spec: endpt.DNSEndpointSpec{
			Endpoints: endpoints,
		},
	}
}

func createEndpoint(hostname string, timeToLive int64, target []string) (endpoint *endpt.Endpoint) {
	endpoint = new(endpt.Endpoint)
	endpoint.DNSName = hostname
	endpoint.Targets = target
	endpoint.RecordTTL = endpt.TTL(timeToLive)
	endpoint.RecordType = "CNAME"
	return endpoint
}

func replaceDomain(hostname string) string {
	var re = regexp.MustCompile(`(^|[^_])\bweb.cern.ch\b([^_]|$)`)
	return re.ReplaceAllString(hostname, `.app.cern.ch`)
}
