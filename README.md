# dns-synchronizer

```
go build main.go types.go aux.go
docker image build -t dns-synchronizer:v1.00 .
docker tag dns-synchronizer:v1.00 gitlab-registry.cern.ch/estevesm/dns-synchronizer:v1.00
docker push gitlab-registry.cern.ch/estevesm/dns-synchronizer:v1.00

export HELM_VERSION=2.13.1
export NAMESPACE=paas-infra-dnsmanager
export TILLER_NAMESPACE=${NAMESPACE}
export HELM_RELEASE_NAME=dns-synchronizer

curl "https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar zx
mv --force linux-amd64/{helm,tiller} /usr/bin/

oc login -u system:admin
oc label node localhost node-role.kubernetes.io/master=true
helm init --client-only
helm plugin install https://github.com/adamreese/helm-local
command -v which || yum install -y whifch
helm local start
helm local status
export HELM_HOST=":44134"
helm template prerequisites/ | oc create -f -
oc project paas-infra-dnsmanager
oc login --token $(oc serviceaccounts get-token tiller)
wget http://service-oracle-tnsnames.web.cern.ch/service-oracle-tnsnames/tnsnames.ora
oc create configmap oracle-config --from-file=tnsnames.ora
helm upgrade ${HELM_RELEASE_NAME} chart/ --install --namespace ${NAMESPACE}
```